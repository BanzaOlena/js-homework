/** @format */

"use strict";

let number = prompt("Enter number");

while (number === null || number === "" || isNaN(number)) {
  number = prompt("Enter number");
}

function calcFactorial(a) {
  if (a < 0) return;
  if (a === 0) return 1;
  return a * calcFactorial(a - 1);
}

console.log(calcFactorial(number));
