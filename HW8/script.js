/** @format */

"use strict";

let bagColor = document.querySelectorAll("p");
for (let value of bagColor) {
  value.style.backgroundColor = "#ff0000";
}

let list = document.querySelector("#optionsList");
console.log(list);
console.log(list.parentElement);
if (list.hasChildNodes()) {
  let value = list.childNodes;
  for (let i of value) {
    console.log(`name: ${i.nodeName}, type: ${i.nodeType}`);
  }
}

let test = document.getElementById("testParagraph");
test.innerText = "This is a paragraph";

let head = document.querySelectorAll(".main-header > div > ul >li");
for (let el of head) {
  console.log(el);
  el.classList.add("nav-item");
}

let title = document.querySelectorAll(".section-title");
for (let el of title) {
  el.classList.remove("section-title");
}
