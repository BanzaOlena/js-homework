/** @format */

"use strict";

function filterBy(arr, type) {
  return arr.filter((item) => typeof item !== type);
}
