<!-- @format -->

1. setTimeout() - вызывает функцию только один раз через определенное время(т.е. с определенной задержкой), a setInterval() вызывает функцию постоянно через определенное время.
2. Если в setTimeout() передать нулевую задержку, то функция выполнится на столько быстро, на сколько сможет. Сначала завершится текущий код, а уже затем выполнится функция.
3. clearInterval() необходимо вызывать для того, что бы остановить выполнение setInterval().
