/** @format */

"use strict";

const imgs = document.querySelectorAll("img");

const btnWrap = document.createElement("div");
btnWrap.classList.add("btn-wrap");
document.body.append(btnWrap);

const start = document.createElement("button");
start.innerText = "Start";
start.classList.add("button");
btnWrap.append(start);

const paused = document.createElement("button");
paused.innerText = "Припинити";
paused.classList.add("button");

const move = document.createElement("button");
move.innerText = "Відновити показ";
move.classList.add("button");
move.setAttribute("disabled", "true");

let currentIndex = 0;
imgs[0].style.display = "block";
let timer;

function moveNetImage() {
  imgs[currentIndex].style.display = "none";
  currentIndex++;
  if (currentIndex >= imgs.length) currentIndex = 0;
  imgs[currentIndex].style.display = "block";
}

start.addEventListener("click", () => {
  timer = setInterval(moveNetImage, 3000);
  btnWrap.append(paused);
  btnWrap.append(move);
  start.remove();
});

move.addEventListener("click", () => {
  timer = setInterval(moveNetImage, 3000);
  move.setAttribute("disabled", "true");
});

paused.addEventListener("click", () => {
  clearInterval(timer);
  move.removeAttribute("disabled");
});
