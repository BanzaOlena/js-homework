/** @format */

"use strict";

const f0 = +prompt("Введите первое число последовательности");
const f1 = +prompt("Введите первое число последовательности");
const n = +prompt(
  "Введите порядковый номер числа Фибоначчи, которое надо найти"
);

function getFib(f0, f1, n) {
  let result = f0 + f1;
  if (n <= 0) {
    console.log("Error!");
    return;
  }
  if (n === 1) {
    return result;
  }
  if (n > 1) {
    result = getFib(f1, result, n - 1);
  }
  return result;
}

console.log(getFib(f0, f1, n));
