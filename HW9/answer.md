<!-- @format -->

1. Новый тег на странице можно создать при помощи document.createElement("тег"), затем нужно этот элемент вставить на страницу отоносительно какого-либо элемента при помощи after, before, prepend, append. Можно сразу создать тег и встать его одним действием при помощи insertAjasmentHTML(where, HTML). Здесь нужно в первом параметре выбрать место куда вставлять относительно какого-либо элемента и в HTML уже непосредственно пишется тег и его содержимое.
2. Первый парамент указывает место куда относительно элемента вставить тег:
   beforebegin перед элементом
   afterbegin внутри элемента вначале
   beforeend внутри элемента в конце
   afterend после элемента.
3. Для удаления элемента elem используется elem.remove()
