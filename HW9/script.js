/** @format */

"use strict";

function getList(arr, elem = document.body) {
  let fragment = document.createDocumentFragment();
  let list = document.createElement("ul");
  for (let i = 0; i < arr.length; i++) {
    const item = document.createElement("li");
    fragment.append(item);
    item.textContent = arr[i];
  }
  list.append(fragment);
  elem.append(list);
}
