/** @format */

"use strict";

function RecursCopyObject(objResult, objSourse) {
  for (let key in objSourse) {
    if ((typeof objSourse[key] !== "object" || objSourse[key]) !== null) {
      objResult[key] = objSourse[key];
    } else {
      objResult[key] = copyObject(objSourse[key]);
    }
  }
  return objResult;
}

function copyObject(obj) {
  let result = {};
  for (let key in obj) {
    if ((typeof obj[key] !== "object" || obj[key]) !== null) {
      result[key] = obj[key];
    } else {
      result[key] = copyObject(obj[key]);
    }
  }
  return result;
}

let listSourse = {
  value: 1,
  next1: {
    value: 2,
    next2: {
      value: 3,
      next3: {
        value: 4,
        next4: null,
      },
    },
  },
};
let result = {};

RecursCopyObject(result, listSourse);
console.log(result);
