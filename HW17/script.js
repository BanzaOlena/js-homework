/** @format */

const student = {};
let name = prompt("Enter your name");
let lastName = prompt("Enter your last name");
student.firstName = name;
student.lastName = lastName;
const tabel = {};
let lesson;
let grade;
do {
  lesson = prompt("Enter lesson");
  grade = +prompt("Enter grade");
  if (lesson && grade) {
    tabel[lesson] = grade;
  }
} while (lesson !== null && grade !== null);

student.tabel = tabel;

let badGrad = 0;
for (let key in student.tabel) {
  if (student.tabel[key] < 4) {
    badGrad += 1;
  }
}
if (badGrad === 0) {
  console.log("Студент переведен на наступний курс");
}
let sum = 0;
let val = 0;
for (let key in student.tabel) {
  sum += student.tabel[key];
  val++;
}
if (sum / val > 7) {
  console.log("Студенту призначено стипендію");
}
