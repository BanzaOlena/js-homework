/** @format */

"use strict";

const icons = document.querySelectorAll(".icon-password");

icons.forEach((el) => {
  el.addEventListener("click", (event) => {
    showPassword(event);
  });
});

function showPassword(event) {
  let target = event.target;
  let dataTarget = target.getAttribute("data-target");

  let input;
  if (dataTarget === "#firstPassword" || dataTarget === "#firstPasswordClose") {
    input = document.querySelector("#firstPassword");
  }
  if (
    dataTarget === "#secondPassword" ||
    dataTarget === "#secondPasswordClose"
  ) {
    input = document.querySelector("#secondPassword");
  }

  let eyeClose = document.querySelectorAll(".fa-eye-slash");
  let eyeOpen = document.querySelectorAll(".fa-eye");

  if (dataTarget === "#firstPassword") {
    target.style.display = "none";
    eyeClose[0].style.display = "block";
    input.type = "text";
  }
  if (dataTarget === "#secondPassword") {
    target.style.display = "none";
    eyeClose[1].style.display = "block";
    input.type = "text";
  }
  if (dataTarget === "#firstPasswordClose") {
    target.style.display = "none";
    eyeOpen[0].style.display = "block";
    input.type = "password";
  }
  if (dataTarget === "#secondPasswordClose") {
    eyeOpen[1].style.display = "block";
    input.type = "password";
    target.style.display = "none";
  }
}

const form = document.querySelector(".password-form");

form.addEventListener("submit", checkPass);

const p = document.createElement("p");
p.style.color = "red";
p.innerText = "Потрібно ввести однакові значення";

function checkPass(event) {
  event.preventDefault();
  let inputs = document.querySelectorAll("input");
  console.log(inputs[0].value);
  console.log(inputs[1].value);
  if (inputs[0].value === "" || inputs[1].value === "") {
    alert("enter password");
    return;
  }
  if (inputs[0].value === inputs[1].value) {
    p.innerText = "";
    alert("You are welcome");
  } else {
    p.innerText = "Потрібно ввести однакові значення";
    inputs[1].after(p);
  }
}
