/** @format */

"use strict";

document.body.addEventListener("keydown", (event) => {
  let buttons = document.querySelectorAll(".btn");
  let target = event.key.toLowerCase();
  buttons.forEach((el) => {
    el.classList.remove("enter");
    if (el.innerText.toLowerCase() === target) {
      el.classList.add("enter");
    }
  });
});
