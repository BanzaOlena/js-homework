/** @format */

"use strict";

let num = prompt("Enter number");

while (
  isNaN(num) ||
  num === null ||
  num === "" ||
  !Number.isInteger(Number(num))
) {
  num = prompt("Enter number");
}

if (num < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= num; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}
