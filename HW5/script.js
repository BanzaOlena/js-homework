/** @format */

function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your last name"),
    getLogin: function () {
      let rez = this.firstName[0] + this.lastName;
      return rez.toLowerCase();
    },
    SetFirstName: function (newValue) {
      Object.defineProperty(this, "firstName", {
        value: newValue,
      });
    },
    SetLastName: function (newValue) {
      Object.defineProperty(this, "lastName", {
        value: newValue,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    writable: false,
  });

  Object.defineProperty(newUser, "lastName", {
    writable: false,
  });

  return newUser;
}

newUser = createNewUser();

console.log(newUser.getLogin());
