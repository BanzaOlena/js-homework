/** @format */

"use strict";

let number1 = prompt("Enter number1");
let number2 = prompt("Enter number2");

while (
  number1 === null ||
  number2 === null ||
  number1 === "" ||
  number2 === "" ||
  isNaN(number1) ||
  isNaN(number2)
) {
  number1 = prompt("Enter number1", number1);
  number2 = prompt("Enter number2", number2);
}

let operation = prompt("Enter operation");

function calc(num1, num2, userOperation) {
  switch (userOperation) {
    case "+":
      return num1 + num2;
      break;
    case "-":
      return num1 - num2;
      break;
    case "*":
      return num1 * num2;
      break;
    case "/":
      return num1 / num2;
  }
}
console.log(calc(number1, number2, operation));
