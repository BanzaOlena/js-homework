/** @format */

/** @format */

function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your last name"),
    birthday: prompt("Enter your birthday", "01.01.1995"),
    getAge: function () {
      let ms =
        Date.now() - Date.parse(this.birthday.split(".").reverse().join("-"));

      return `${this.firstName} ${Math.floor(
        ms / (1000 * 60 * 60 * 24 * 30 * 12)
      )} years`;
    },
    getPassword: function () {
      let pas =
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6);
      return pas;
    },
    getLogin: function () {
      let rez = this.firstName[0] + this.lastName;
      return rez.toLowerCase();
    },
    SetFirstName: function (newValue) {
      Object.defineProperty(this, "firstName", {
        value: newValue,
      });
    },
    SetLastName: function (newValue) {
      Object.defineProperty(this, "lastName", {
        value: newValue,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    writable: false,
  });

  Object.defineProperty(newUser, "lastName", {
    writable: false,
  });

  return newUser;
}

newUser = createNewUser();

console.log(newUser.getAge());
console.log(newUser.getPassword());
