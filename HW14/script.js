/** @format */

"use strict";

let switchMode = document.querySelector("#switchMode");

let activeTheme = localStorage.getItem("theme");

setTheme(activeTheme);

switchMode.addEventListener("click", switchTheme);

function switchTheme() {
  activeTheme = localStorage.getItem("theme");
  if (activeTheme === "light") {
    setTheme("dark");
  }
  if (activeTheme === "dark") {
    setTheme("light");
  }
}

function setTheme(themeName) {
  let theme = document.querySelector("#theme");

  if (themeName === null || themeName === "light") {
    theme.setAttribute("href", "./style/light-mode.css");
    localStorage.setItem("theme", "light");
  }

  if (themeName === "dark") {
    theme.setAttribute("href", "./style/dark-mode.css");
    localStorage.setItem("theme", "dark");
  }
}
