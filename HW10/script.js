/** @format */

"use strict";

const tabsItems = document.querySelectorAll(".tabs>li");
const articles = document.querySelectorAll(".tabs-content>li");

// const tabs = document.querySelector(".tabs");
// tabs.addEventListener("click", (event) => {
//   let target = event.target;
//   if (target.tagName === "LI") {
//     console.log("test");
//     let active = document.querySelector(".active");
//     let dataCourent = target.getAttribute("data-tab");
//     let article = document.querySelector(
//       `.tabs-content>li[data-tab = ${dataCourent}]`
//     );
//     let articleActive = document.querySelector(
//       `.tabs-content>li[data-tab = ${active.getAttribute("data-tab")}]`
//     );
//     if (active !== target) {
//       active.classList.remove("active");
//       target.classList.add("active");
//       article.classList.remove("hidden");
//       articleActive.classList.add("hidden");
//     }
//   } else {
//     return;
//   }
// });

tabsItems.forEach((elem) => {
  elem.addEventListener("click", () => {
    let tabs = elem;
    let activeIndex = 0;

    articles.forEach((elem) => {
      elem.classList.add("hidden");
    });

    tabsItems.forEach((elem) => {
      elem.classList.remove("active");
    });

    tabs.classList.add("active");

    tabsItems.forEach((el) => {
      if (el.classList.contains("active")) {
        articles[activeIndex].classList.remove("hidden");
      }
      activeIndex++;
    });
  });
});
