/** @format */

"use stict";

const images = document.querySelectorAll(".img");
const btn = document.querySelectorAll(".button");
let courentIndex = 0;
images[0].style.display = "block";

btn.forEach((element) => {
  element.addEventListener("click", (ev) => {
    let target = ev.target;
    if (target.classList.contains("move-left")) {
      if (courentIndex === 0) {
        images[courentIndex].style.display = "none";
        courentIndex = images.length - 1;
        images[courentIndex].style.display = "block";
        return;
      }
      images[courentIndex].style.display = "none";
      courentIndex--;
      images[courentIndex].style.display = "block";
    }
    if (target.classList.contains("move-right")) {
      if (courentIndex === images.length - 1) {
        images[courentIndex].style.display = "none";
        courentIndex = 0;
        images[courentIndex].style.display = "block";
        return;
      }
      images[courentIndex].style.display = "none";
      courentIndex++;
      images[courentIndex].style.display = "block";
    }
  });
});
