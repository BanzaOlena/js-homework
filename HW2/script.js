/** @format */

"use strict";

let userName = prompt("What is your name?");
let userAge = prompt("How old are you?");

while (userName === null || userName === "") {
  userName = prompt("Enter your name", userName);
}
while (isNaN(userAge) || userAge === null || userAge === "") {
  userAge = prompt("Enter your age", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (18 < userAge && userAge <= 22) {
  let userAnswer = confirm("Are you sure you want to continue?");

  if (userAnswer === true) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${userName}`);
}
